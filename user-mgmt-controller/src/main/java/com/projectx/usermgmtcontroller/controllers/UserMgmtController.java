package com.projectx.usermgmtcontroller.controllers;

import com.projectx.usermgmtservice.model.UserVO;

import java.util.Optional;

import com.projectx.usermgmtservice.api.IUserMgmtService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Mono;


@RestController
@RequestMapping("/v1")
@Slf4j
public class UserMgmtController {
    private IUserMgmtService userService;

    @Autowired
    public UserMgmtController(IUserMgmtService userService){
        this.userService=userService;
    } 

    @RequestMapping(value="/users", method=RequestMethod.POST)
    public Mono<UserVO> addUser(@RequestBody UserVO user) {
        log.debug("user : "+user);
        return userService.addUser(user);
    }
    

    @RequestMapping(value="/users/{id}", method=RequestMethod.PUT)
    public Mono<UserVO> updateUser(@PathVariable("id") String id, @RequestBody UserVO user) {
        log.debug("user : "+user);
        user.setId(id);
        return userService.updateUser(user);
    }

    @RequestMapping(value="/users/{id}", method=RequestMethod.GET)
    public Mono<UserVO> getUser(@PathVariable("id") String id) {
        log.debug("get user with id: "+id);
        return userService.getUser(id);
    }

    @RequestMapping(value="/users", method=RequestMethod.DELETE)
    public Mono<Void> deleteUser(@RequestBody UserVO user) {
        log.debug("user : "+user);
        return userService.deleteUser(user);
    }

    @RequestMapping(value="/users/{id}", method=RequestMethod.DELETE)
    public Mono<Void> deleteUserById(@PathVariable("id") String id) {
        log.debug("deleting user with id : "+id);
        return userService.deleteUserById(id);
    }
}
