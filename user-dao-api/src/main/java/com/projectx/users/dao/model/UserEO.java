package com.projectx.users.dao.model;

import com.common.dao.entities.AbstractEO;

import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Data;

@Data
@Document
public class UserEO extends AbstractEO {
    
    private String firstname;
    private String lastname;    
    private String email;
    
    private AddressEO billing,shipping;
}
