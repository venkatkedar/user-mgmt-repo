package com.projectx.users.dao.model;

import com.common.dao.entities.AbstractEO;

import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Data;

@Data
@Document
public class AddressEO extends AbstractEO{
    private String address1;
    private String address2;
    private String city;
    private String state;
    private String country;    
}
