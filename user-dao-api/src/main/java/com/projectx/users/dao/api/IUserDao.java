package com.projectx.users.dao.api;

import com.common.dao.api.ReactiveBaseDao;
import com.projectx.users.dao.model.UserEO;

import reactor.core.publisher.Mono;

public interface IUserDao extends ReactiveBaseDao<UserEO>{
    
}
