package com.projectx.usermgmtservice.api;


import com.common.service.exceptions.ServiceException;
import com.projectx.usermgmtservice.model.UserVO;

import reactor.core.publisher.Mono;

public interface IUserMgmtService {
    Mono<UserVO> addUser(UserVO user) throws ServiceException;
    Mono<UserVO> updateUser(UserVO user) throws ServiceException;
    Mono<Void> deleteUser(UserVO user) throws ServiceException;
    Mono<Void> deleteUserById(String id)throws ServiceException;
    Mono<UserVO> getUser(String userId) throws ServiceException;
}
