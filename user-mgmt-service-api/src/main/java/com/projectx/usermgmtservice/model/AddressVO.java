package com.projectx.usermgmtservice.model;

import com.common.service.valueobjects.AbstractVO;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.experimental.NonFinal;

@Data
@NoArgsConstructor
public class AddressVO extends AbstractVO{
    @NonFinal
    private String address1;
    private String address2;
    @NonNull
    private String city;
    @NonNull
    private String state;
    @NonNull
    private String country;
}
