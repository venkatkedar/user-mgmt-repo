package com.projectx.usermgmtservice.impl;

import java.util.Date;

import com.common.dao.exceptions.EntityNotFoundException;
import com.common.service.exceptions.NotFoundException;
import com.common.service.exceptions.ServiceException;
import com.common.utils.converter.Converter;
import com.projectx.usermgmtservice.api.IUserMgmtService;
import com.projectx.usermgmtservice.model.AddressVO;
import com.projectx.usermgmtservice.model.UserVO;
import com.projectx.users.dao.api.IUserDao;
import com.projectx.users.dao.model.AddressEO;
import com.projectx.users.dao.model.UserEO;
import com.projectx.users.dao.repository.UserEOReactiveRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Mono;

@NoArgsConstructor
@Slf4j
@Service
public class UserMgmtServiceImpl implements IUserMgmtService {

    private IUserDao userdao;

    @Autowired
    public UserMgmtServiceImpl(IUserDao userdao){
        this.userdao=userdao;       
    }

    @Override
    public Mono<UserVO> addUser(UserVO user) throws ServiceException{
        log.info("user in service :"+user);
        Date d=new Date();
        user.setLastUpdatedTime(d);
        user.getBilling().setLastUpdatedTime(d);
        user.getShipping().setLastUpdatedTime(d);
        UserEO uEO=Converter.convertUToV(user, UserEO.class);
        Mono<UserEO> userEO= userdao.add(uEO);
        
        return userEO.map(f->Converter.convertUToV(f, UserVO.class));
    }

    @Override
    public Mono<UserVO> updateUser(UserVO user) throws ServiceException{
        log.info("updateUser :"+user);
        Date d=new Date();
        user.setLastUpdatedTime(d);
        user.getBilling().setLastUpdatedTime(d);
        user.getShipping().setLastUpdatedTime(d);
        UserEO uEO=Converter.convertUToV(user, UserEO.class);
        Mono<UserEO> userEO= userdao.update(uEO);
        
        return userEO.map(f->{
            return Converter.convertUToV(f, UserVO.class);
        });
    }

    @Override
    public Mono<Void> deleteUser(UserVO user) throws ServiceException{
        log.info("deleteUser :"+user);
        UserEO uEO=Converter.convertUToV(user, UserEO.class);
        return userdao.delete(uEO);
    }

    @Override
    public Mono<Void> deleteUserById(String id) throws ServiceException{
        log.info("deleteUserById :"+id);
        return userdao.deleteById(id);
    }

    @Override
    public Mono<UserVO> getUser(String userId) throws ServiceException{
        log.info("getUserById :"+userId);
        Mono<UserEO> uEO= userdao.findById(userId)
                                 .doOnError(error -> {
                                     if(error instanceof EntityNotFoundException) 
                                        throw new NotFoundException(error.getMessage());
                                     else throw new ServiceException(error.getMessage());
                                 });
        return uEO.map(user->Converter.convertUToV(user, UserVO.class));
    }

   

    
}
