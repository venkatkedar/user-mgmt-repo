package com.projectx.usermgmtservice.impl;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
public class ClassA {
    ClassB classB;
    InterfaceC intC;

    public String add(String name){
        String output=intC.add(name);
        return output;
    }


}
