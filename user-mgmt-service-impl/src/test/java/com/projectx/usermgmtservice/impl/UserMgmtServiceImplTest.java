package com.projectx.usermgmtservice.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.Date;

import com.common.utils.converter.Converter;
import com.projectx.usermgmtservice.api.IUserMgmtService;
import com.projectx.usermgmtservice.model.AddressVO;
import com.projectx.usermgmtservice.model.UserVO;
import com.projectx.users.dao.api.IUserDao;
import com.projectx.users.dao.impl.UserDaoImpl;
import com.projectx.users.dao.model.UserEO;
import com.projectx.users.dao.repository.UserEOReactiveRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;

import reactor.core.publisher.Mono;

//@RunWith(MockitoJUnitRunner.class)
public class UserMgmtServiceImplTest {
       
    IUserMgmtService userService;

    //@Mock
    IUserDao dao;
    
    @Mock
    UserEOReactiveRepository repo;

    @Before
    public void setUp(){   
        System.out.println("repo object ="+repo);    
        dao=new UserDaoImpl(repo);
        userService=new UserMgmtServiceImpl(dao);
    }

    //@Test 
    public void testAddUser(){
        UserVO user=createObject();
        UserEO userEO=Converter.convertUToV(user, UserEO.class);
        Mono<UserEO> monouserEO=Mono.just(userEO);
        //when(dao).thenReturn(new UserDaoImpl(repo));
        when(dao.add(userEO)).thenReturn(monouserEO);
        Mono<UserVO> monoActual=userService.addUser(user);
        UserVO actual=monoActual.block();
        assertEquals(user, actual);
    }

    //@Test
    public void testUpdateUser(){
        UserVO user=createObject();
        UserEO userEO=Converter.convertUToV(user, UserEO.class);
        Mono<UserEO> monouserEO=Mono.just(userEO);
        when(dao.add(userEO)).thenReturn(monouserEO);
        Mono<UserVO> monoActual=userService.updateUser(user);
        UserVO actual=monoActual.block();
        assertEquals(user, actual);
    }

    //@Test
    public void testGetUser(){
        UserVO user=createObject();
        UserEO userEO=Converter.convertUToV(user, UserEO.class);
        Mono<UserEO> monouserEO=Mono.just(userEO);
        when(dao.findById(user.getId())).thenReturn(monouserEO);
        Mono<UserVO> monoActual=userService.getUser(user.getId());
        UserVO actual=monoActual.block();
        assertEquals(user, actual);
    }

    //@Test
    public void testDeleteUser(){
        UserVO user=createObject();
        UserEO userEO=Converter.convertUToV(user, UserEO.class);
        Mono<Void> monovoid=Mono.empty();
        when(dao.delete(userEO)).thenReturn(monovoid);
        Mono<Void> monoActual=userService.deleteUser(user);
        Void actual=monoActual.block();
        assertEquals(Void.class,actual.getClass());
    }

    private UserVO createObject(){
        UserVO user=new UserVO();
        user.setId("1441");
        user.setFirstname("John");
        user.setLastname("Patro");
        user.setEmail("john.patro@g.com");
        AddressVO addr=new AddressVO();
        addr.setAddress1("address1");
        addr.setAddress2("address2");
        addr.setCity("RWC");
        addr.setCountry("USA");
        addr.setId("1442");
        addr.setLastUpdatedTime(new Date());
        addr.setState("CA");
        user.setBilling(addr);
        user.setShipping(addr);
        return user;
    }
}
