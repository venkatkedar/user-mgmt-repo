package com.projectx.usermgmtservice.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import com.projectx.users.dao.impl.UserDaoImpl;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class ClassATest {
    @Mock
    ClassB classB;

    @Mock
    InterfaceC intC;
    
    @Before
    public void setUp(){   
        System.out.println("intc object ="+intC);    
        
    }

    @Test
    public void testAdd(){
        ClassA classA=new ClassA(classB,intC);
        String name="Kedar";
        
        when(intC.add(name)).thenReturn(name+"1");
        String actual=classA.add(name);
        assertEquals(name+1, actual);
    }
}
