package com.projectx.users.dao.impl;

import com.common.dao.spring.counters.CounterService;
import com.common.dao.spring.impl.AbstractSpringDataReactiveDaoImpl;
import com.projectx.users.dao.api.IUserDao;
import com.projectx.users.dao.model.UserEO;
import com.projectx.users.dao.repository.UserEOReactiveRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Mono;

@Data
@Repository
@Slf4j
public class UserDaoImpl extends AbstractSpringDataReactiveDaoImpl<UserEO,UserEOReactiveRepository> implements IUserDao {
    private UserEOReactiveRepository repository;
    @Autowired
    private CounterService counterService;

    @Value("${collection.users}")
    private String collectionName;

    @Autowired
    public UserDaoImpl(UserEOReactiveRepository repository){
        super(repository);
        this.repository=repository;
    }
    
    @Override
    public Mono<UserEO> add(UserEO entity) {
        //entity.setId(counterService.getNextSequenceFor(collectionName));
        //return super.add(entity);
        return Mono.just(entity);
    }
}
