package com.projectx.users.dao.repository;

import com.common.dao.spring.api.AbstractEORepository;
import com.projectx.users.dao.model.UserEO;

public interface UserEORepository extends AbstractEORepository<UserEO>{
    
}
