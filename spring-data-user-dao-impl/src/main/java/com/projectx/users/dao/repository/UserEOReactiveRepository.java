package com.projectx.users.dao.repository;

import com.common.dao.spring.api.AbstractEOReactiveRepository;
import com.projectx.users.dao.model.UserEO;

import org.springframework.stereotype.Repository;

@Repository
public interface UserEOReactiveRepository extends AbstractEOReactiveRepository<UserEO>{
    
}
